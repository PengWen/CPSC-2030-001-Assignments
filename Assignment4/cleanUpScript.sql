-- delete a particular condition

DELETE FROM pokemon
WHERE condition;


-- delete the whole table

DELETE * FROM pokemon;
