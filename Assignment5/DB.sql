-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 22, 2018 at 12:57 AM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `pokedex`
--

-- --------------------------------------------------------

--
-- Table structure for table `pokedex_type`
--

CREATE TABLE `pokedex_type` (
  `pid` int(11) NOT NULL,
  `tid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pokedex_type`
--

INSERT INTO `pokedex_type` (`pid`, `tid`) VALUES
(396, 1),
(397, 1),
(398, 1),
(399, 1),
(400, 1),
(424, 1),
(427, 1),
(428, 1),
(431, 1),
(432, 1),
(440, 1),
(441, 1),
(446, 1),
(463, 1),
(391, 2),
(392, 2),
(447, 2),
(448, 2),
(453, 2),
(454, 2),
(396, 3),
(397, 3),
(398, 3),
(414, 3),
(415, 3),
(416, 3),
(425, 3),
(426, 3),
(430, 3),
(441, 3),
(458, 3),
(406, 4),
(407, 4),
(434, 4),
(435, 4),
(451, 4),
(452, 4),
(453, 4),
(454, 4),
(389, 5),
(423, 5),
(443, 5),
(444, 5),
(445, 5),
(449, 5),
(450, 5),
(464, 5),
(408, 6),
(409, 6),
(410, 6),
(411, 6),
(438, 6),
(464, 6),
(401, 7),
(402, 7),
(412, 7),
(413, 7),
(414, 7),
(415, 7),
(416, 7),
(451, 7),
(425, 8),
(426, 8),
(429, 8),
(442, 8),
(395, 9),
(410, 9),
(411, 9),
(436, 9),
(437, 9),
(448, 9),
(462, 9),
(390, 10),
(391, 10),
(392, 10),
(393, 11),
(394, 11),
(395, 11),
(400, 11),
(418, 11),
(419, 11),
(422, 11),
(423, 11),
(456, 11),
(457, 11),
(458, 11),
(387, 12),
(388, 12),
(389, 12),
(406, 12),
(407, 12),
(413, 12),
(420, 12),
(421, 12),
(455, 12),
(459, 12),
(460, 12),
(465, 12),
(403, 13),
(404, 13),
(405, 13),
(417, 13),
(462, 13),
(433, 14),
(436, 14),
(437, 14),
(439, 14),
(459, 15),
(460, 15),
(461, 15),
(443, 16),
(444, 16),
(445, 16),
(439, 17),
(430, 18),
(434, 18),
(435, 18),
(442, 18),
(452, 18),
(461, 18);

-- --------------------------------------------------------

--
-- Table structure for table `pokemon`
--

CREATE TABLE `pokemon` (
  `pokedex` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `hp` int(11) NOT NULL,
  `atk` int(11) NOT NULL,
  `def` int(11) NOT NULL,
  `sat` int(11) NOT NULL,
  `sdf` int(11) NOT NULL,
  `spd` int(11) NOT NULL,
  `bst` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pokemon`
--

INSERT INTO `pokemon` (`pokedex`, `name`, `hp`, `atk`, `def`, `sat`, `sdf`, `spd`, `bst`) VALUES
(387, 'Turtwig', 55, 68, 64, 45, 55, 31, 318),
(388, 'Grotle', 75, 89, 85, 55, 65, 36, 405),
(389, 'Torterra', 95, 109, 105, 75, 85, 56, 525),
(390, 'Chimchar', 44, 58, 44, 58, 44, 61, 309),
(391, 'Monferno', 64, 78, 52, 78, 52, 81, 405),
(392, 'Infernape', 76, 104, 71, 104, 71, 108, 534),
(393, 'Piplup', 53, 51, 53, 61, 56, 40, 314),
(394, 'Prinplup', 64, 66, 68, 81, 76, 50, 405),
(395, 'Empoleon', 84, 86, 88, 111, 101, 60, 530),
(396, 'Starly', 40, 55, 30, 30, 30, 60, 245),
(397, 'Staravia', 55, 75, 50, 40, 40, 80, 340),
(398, 'Staraptor', 85, 120, 70, 50, 60, 100, 485),
(399, 'Bidoof', 59, 45, 40, 35, 40, 31, 250),
(400, 'Bibarel', 79, 85, 60, 55, 60, 71, 410),
(401, 'Kricketot', 37, 25, 41, 25, 41, 25, 194),
(402, 'Kricketune', 77, 85, 51, 55, 51, 65, 384),
(403, 'Shinx', 45, 65, 34, 40, 34, 45, 263),
(404, 'Luxio', 60, 85, 49, 60, 49, 60, 363),
(405, 'Luxray', 80, 120, 79, 95, 79, 70, 523),
(406, 'Budew', 40, 30, 35, 50, 70, 55, 280),
(407, 'Roserade', 60, 70, 65, 125, 105, 90, 515),
(408, 'Cranidos', 67, 125, 40, 30, 30, 58, 350),
(409, 'Rampardos', 97, 165, 60, 65, 50, 58, 495),
(410, 'Shieldon', 30, 42, 118, 42, 88, 30, 350),
(411, 'Bastiodon', 60, 52, 168, 47, 138, 30, 495),
(412, 'Burmy', 40, 29, 45, 29, 45, 36, 224),
(413, 'Wormadam', 60, 59, 85, 79, 105, 36, 424),
(414, 'Mothim', 70, 94, 50, 94, 50, 66, 424),
(415, 'Combee', 30, 30, 42, 30, 42, 70, 244),
(416, 'Vespiquen', 70, 80, 102, 80, 102, 40, 474),
(417, 'Pachirisu', 60, 45, 70, 45, 90, 95, 405),
(418, 'Buizel', 55, 65, 35, 60, 30, 85, 330),
(419, 'Floatzel', 85, 105, 55, 85, 50, 115, 495),
(420, 'Cherubi', 45, 35, 45, 62, 53, 35, 275),
(421, 'Cherrim', 70, 60, 70, 87, 78, 85, 450),
(422, 'Shellos', 76, 48, 48, 57, 62, 34, 325),
(423, 'Gastrodon', 111, 83, 68, 92, 82, 39, 475),
(424, 'Ambipom', 75, 100, 66, 60, 66, 115, 482),
(425, 'Drifloon', 90, 50, 34, 60, 44, 70, 348),
(426, 'Drifblim', 150, 80, 44, 90, 54, 80, 498),
(427, 'Buneary', 55, 66, 44, 44, 56, 85, 350),
(428, 'Lopunny', 65, 76, 84, 54, 96, 105, 480),
(429, 'Mismagius', 60, 60, 60, 105, 105, 105, 495),
(430, 'Honchkrow', 100, 125, 52, 105, 52, 71, 505),
(431, 'Glameow', 49, 55, 42, 42, 37, 85, 310),
(432, 'Purugly', 71, 82, 64, 64, 59, 112, 452),
(433, 'Chingling', 45, 30, 50, 65, 50, 45, 285),
(434, 'Stunky', 63, 63, 47, 41, 41, 74, 329),
(435, 'Skuntank', 103, 93, 67, 71, 61, 84, 479),
(436, 'Bronzor', 57, 24, 86, 24, 86, 23, 300),
(437, 'Bronzong', 67, 89, 116, 79, 116, 33, 500),
(438, 'Bonsly', 50, 80, 95, 10, 45, 10, 290),
(439, 'Mime Jr.', 20, 25, 45, 70, 90, 60, 310),
(440, 'Happiny', 100, 5, 5, 15, 65, 30, 220),
(441, 'Chatot', 76, 65, 45, 92, 42, 91, 411),
(442, 'Spiritomb', 50, 92, 108, 92, 108, 35, 485),
(443, 'Gible', 58, 70, 45, 40, 45, 42, 300),
(444, 'Gabite', 68, 90, 65, 50, 55, 82, 410),
(445, 'Garchomp', 108, 130, 95, 80, 85, 102, 600),
(446, 'Munchlax', 135, 85, 40, 40, 85, 5, 390),
(447, 'Riolu', 40, 70, 40, 35, 40, 60, 285),
(448, 'Lucario', 70, 110, 70, 115, 70, 90, 525),
(449, 'Hippopotas', 68, 72, 78, 38, 42, 32, 330),
(450, 'Hippowdon', 108, 112, 118, 68, 72, 47, 525),
(451, 'Skorupi', 40, 50, 90, 30, 55, 65, 330),
(452, 'Drapion', 70, 90, 110, 60, 75, 95, 500),
(453, 'Croagunk', 48, 61, 40, 61, 40, 50, 300),
(454, 'Toxicroak', 83, 106, 65, 86, 65, 85, 490),
(455, 'Carnivine', 74, 100, 72, 90, 72, 46, 454),
(456, 'Finneon', 49, 49, 56, 49, 61, 66, 330),
(457, 'Lumineon', 69, 69, 76, 69, 86, 91, 460),
(458, 'Mantyke', 45, 20, 50, 60, 120, 50, 345),
(459, 'Snover', 60, 62, 50, 62, 60, 40, 334),
(460, 'Abomasnow', 90, 92, 75, 92, 85, 60, 494),
(461, 'Weavile', 70, 120, 65, 45, 85, 125, 510),
(462, 'Magnezone', 70, 70, 115, 130, 90, 60, 535),
(463, 'Lickilicky', 110, 85, 95, 80, 95, 50, 515),
(464, 'Rhyperior', 115, 140, 130, 55, 55, 40, 535),
(465, 'Tangrowth', 100, 100, 125, 110, 50, 50, 535);

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `strongAgainst` varchar(100) NOT NULL,
  `weakAgainst` varchar(100) NOT NULL,
  `resistantTo` varchar(100) NOT NULL,
  `vulnerableTo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `type`, `strongAgainst`, `weakAgainst`, `resistantTo`, `vulnerableTo`) VALUES
(1, 'Normal', '', 'Rock, Ghost, Steel', 'Ghost', 'Fighting'),
(2, 'Fight', 'Normal, Rock, Steel, Ice, Dark', 'Flying, Poison, Psychic, Bug, Ghost, Fairy', 'Rock, Bug, Dark', 'Flying, Psychic, Fairy'),
(3, 'Flying', 'Fighting, Bug, Grass', 'Rock, Steel, Electric', 'Fighting, Ground, Bug, Grass', 'Rock, Electric, Ice'),
(4, 'Poison', 'Grass, Fairy', 'Poison, Ground, Rock, Ghost, Steel', 'Fighting, Poison, Grass, Fairy', 'Ground, Psychic'),
(5, 'Ground', 'Poison, Rock, Steel, Fire, Electric', 'Flying, Bug, Grass', 'Poison, Rock, Electric', 'Water, Grass, Ice'),
(6, 'Rock', 'Flying, Bug, Fire, Ice', 'Fighting, Ground, Steel', 'Normal, Flying, Poison, Fire', 'Fighting, Ground, Steel, Water, Grass'),
(7, 'Bug', 'Grass, Psychic, Dark', 'Fighting, Flying, Poison, Ghost, Steel, Fire, Fairy', 'Fighting, Ground, Grass', 'Flying, Rock, Fire'),
(8, 'Ghost', 'Ghost, Psychic', 'Normal, Dark', 'Normal, Fighting, Poison, Bug', 'Ghost, Dark'),
(9, 'Steel', 'Rock, Ice, Fairy', 'Steel, Fire, Water, Electric', 'Normal, Flying, Poison, Rock, Bug, Steel, Grass, Psychic, Ice, Dragon, Fairy', 'Fighting, Ground, Fire'),
(10, 'Fire', 'Bug, Steel, Grass, Ice', 'Rock, Fire, Water, Dragon', 'Bug, Steel, Fire, Grass, Ice', 'Ground, Rock, Water'),
(11, 'Water', 'Ground, Rock, Fire', 'Water, Grass, Dragon', 'Steel, Fire, Water, Ice', 'Grass, Electric'),
(12, 'Grass', 'Ground, Rock, Water', 'Flying, Poison, Bug, Steel, Fire, Grass, Dragon', 'Ground, Water, Grass, Electric', 'Flying, Poison, Bug, Fire, Ice'),
(13, 'Electric', 'Flying, Water', 'Ground, Grass, Electric, Dragon', 'Flying, Steel, Electric', 'Ground'),
(14, 'Psychic', 'Fighting, Poison', 'Steel, Psychic, Dark', 'Fighting, Psychic', 'Bug, Ghost, Dark'),
(15, 'Ice', 'Flying, Ground, Grass, Dragon', 'Steel, Fire, Water, Ice', 'Ice', 'Fighting, Rock, Steel, Fire'),
(16, 'Dragon', 'Dragon', 'Steel, Fairy', 'Fire, Water, Grass, Electric', 'Ice, Dragon, Fairy'),
(17, 'Fairy', 'Fighting, Dragon, Dark', 'Poison, Steel, Fire', 'Fighting, Bug, Dragon, Dark', 'Poison, Steel'),
(18, 'Dark', 'Ghost, Psychic', 'Fighting, Dark, Fairy', 'Ghost, Psychic, Dark', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pokedex_type`
--
ALTER TABLE `pokedex_type`
  ADD PRIMARY KEY (`pid`,`tid`),
  ADD KEY `TID_FK` (`tid`);

--
-- Indexes for table `pokemon`
--
ALTER TABLE `pokemon`
  ADD PRIMARY KEY (`pokedex`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pokemon`
--
ALTER TABLE `pokemon`
  MODIFY `pokedex` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=466;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pokedex_type`
--
ALTER TABLE `pokedex_type`
  ADD CONSTRAINT `PID_FK` FOREIGN KEY (`pid`) REFERENCES `pokemon` (`pokedex`),
  ADD CONSTRAINT `TID_FK` FOREIGN KEY (`tid`) REFERENCES `type` (`id`);
