INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd,bst)
VALUES (387,'Turtwig',55,68,64,45,55,31,318)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (388,'Grotle',75,89,85,55,65,36,405)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (389,'Torterra',95,109,105,75,85,56,525)


INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (390,'Chimchar',44,58,44,58,44,61,309)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (391,'Monferno',64,78,52,78,52,81,405)


INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (392,'Infernape',76,104,71,104,71,108,534)


INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (393,'Piplup',53,51,53,61,56,40,314)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (394,'Prinplup',64,66,68,81,76,50,405)


INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (395,'Empoleon',84,86,88,111,101,60,530)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (396,'Starly',40,55,30,30,30,60,245)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (397,'Staravia',55,75,50,40,40,80,340)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (398,'Staraptor',85,120,70,50,60,100,485)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (399,'Bidoof',59,45,40,35,40,31,250)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (400,'Bibarel',79,85,60,55,60,71,410)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (401,'Kricketot',37,25,41,25,41,25,194)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (402,'Kricketune',77,85,51,55,51,65,384)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (403,'Shinx',45,65,34,40,34,45,263)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (404,'Luxio',60,85,49,60,49,60,363)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (405,'Luxray',80,120,79,95,79,70,523)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (406,'Budew',40,30,35,50,70,55,280)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (407,'Roserade',60,70,65,125,105,90,515)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (408,'Cranidos',67,125,40,30,30,58,350)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (409,'Rampardos',97,165,60,65,50,58,495)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (410,'Shieldon',30,42,118,42,88,30,350)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (411,'Bastiodon',60,52,168,47,138,30,495)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (412,'Burmy',40,29,45,29,45,36,224)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (413,'Wormadam',60,59,85,79,105,36,424)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (414,'Mothim',70,94,50,94,50,66,424)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (415,'Combee',30,30,42,30,42,70,244)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (416,'Vespiquen',70,80,102,80,102,40,474)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (417,'Pachirisu',60,45,70,45,90,95,405)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (418,'Buizel',55,65,35,60,30,85,330)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (419,'Floatzel',85,105,55,85,50,115,495)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (420,'Cherubi',45,35,45,62,53,35,275)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (421,'Cherrim',70,60,70,87,78,85,450)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (421,'Cherrim',70,60,70,87,78,85,450)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (422,'Shellos',76,48,48,57,62,34,325)


INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (423,'Gastrodon',111,83,68,92,82,39,475)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (424,'Ambipom',75,100,66,60,66,115,482)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (425,'Drifloon',90,50,34,60,44,70,348)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (426,'Drifblim',150,80,44,90,54,80,498)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (427,'Buneary',55,66,44,44,56,85,350)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (428,'Lopunny',65,76,84,54,96,105,480)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (429,'Mismagius',60,60,60,105,105,105,495)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (430,'Honchkrow',100,125,52,105,52,71,505)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (431,'Glameow',49,55,42,42,37,85,310)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (432,'Purugly',71,82,64,64,59,112,452)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (433,'Chingling',45,30,50,65,50,45,285)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (434,'Stunky',63,63,47,41,41,74,329)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (435,'Skuntank',103,93,67,71,61,84,479)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (436,'Bronzor',57,24,86,24,86,23,300)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (437,'Bronzong',67,89,116,79,116,33,500)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (438,'Bonsly',50,80,95,10,45,10,290)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (439,'Mime Jr.',20,25,45,70,90,60,310)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (440,'Happiny',100,5,5,15,65,30,220)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (441,'Chatot',76,65,45,92,42,91,411)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (442,'Spiritomb',50,92,108,92,108,35,485)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (443,'Gible',58,70,45,40,45,42,300)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (444,'Gabite',68,90,65,50,55,82,410)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (445,'Garchomp',108,130,95,80,85,102,600)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (445,'Mega Garchomp',108,170,115,120,95,92,700)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (446,'Munchlax',135,85,40,40,85,5,390)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (447,'Riolu',40,70,40,35,40,60,285)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (448,'Lucario',70,110,70,115,70,90,525)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (448,'Mega Lucario',70,145,88,140,70,112,625)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (449,'Hippopotas',68,72,78,38,42,32,330)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (450,'Hippowdon',108,112,118,68,72,47,525)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (451,'Skorupi',40,50,90,30,55,65,330)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (452,'Drapion',70,90,110,60,75,95,500)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (453,'Croagunk',48,61,40,61,40,50,300)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (454,'Toxicroak',83,106,65,86,65,85,490)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (455,'Carnivine',74,100,72,90,72,46,454)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (456,'Finneon',49,49,56,49,61,66,330)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (457,'Lumineon',69,69,76,69,86,91,460)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (458,'Mantyke',45,20,50,60,120,50,345)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (459,'Snover',60,62,50,62,60,40,334)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (460,'Abomasnow',90,92,75,92,85,60,494)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (461,'Weavile',70,120,65,45,85,125,510)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (462,'Magnezone',70,70,115,130,90,60,535)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (463,'Lickilicky',110,85,95,80,95,50,515)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (464,'Rhyperior',115,140,130,55,55,40,535)

INSERT
INTO Pokemon
(pokedex,name,hp,atk,def,sat,sdf,spd, bst)
VALUES (465,'Tangrowth',100,100,125,110,50,50,535)


-----------------------------------------------------


INSERT
INTO type
(id,type,weakAgainst)
VALUES (1,'Normal','Rock, Ghost, Steel')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (2,'Fighting','Flying, Poison, Psychic, Bug, Ghost, Fairy')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (3,'Flying','Rock, Steel, Electric')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (4,'Poison','Poison, Ground, Rock, Ghost, Steel')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (5,'Ground','Flying, Bug, Grass')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (6,'Rock','Fighting, Ground, Steel')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (7,'Bug','Fighting, Flying, Poison, Ghost, Steel, Fire, Fairy')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (8,'Ghost','Normal, Dark')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (9,'Steel','Steel, Fire, Water, Electric')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (10,'Fire','Rock, Fire, Water, Dragon')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (11,'Water','Water, Grass, Dragon')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (12,'Grass','Flying, Poison, Bug, Steel, Fire, Grass, Dragon')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (13,'Electric','Ground, Grass, Electric, Dragon')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (14,'Psychic','Steel, Psychic, Dark')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (15,'Ice','Steel, Fire, Water, Ice')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (16,'Dragon','Steel, Fairy')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (17,'Fairy','Poison, Steel, Fire')

INSERT
INTO type
(id,type,weakAgainst)
VALUES (18,'Dark','Fighting, Dark, Fairy')

-----------------------------------------------------

Queries Script:

1.Find the Bottom 10 Pokémon ranked by speed

SELECT * FROM pokemon
ORDER BY spd DESC
LIMIT 10;

2. Find All Pokémon who is vulnerable to Ground and Resistant to Steel



3. Find All Pokémon who has a BST between 200 to 500 who is weak against water types

SELECT * FROM pokemon
WHERE bst BETWEEN 200 AND 500;

4. Find the Pokémon with the highest Atk, has a Mega evolution form and vulnerable to fire
