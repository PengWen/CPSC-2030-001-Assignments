<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="stylesheet.css">
    <title>Pokemon</title>
</head>
   <body>
        <h1>Pokemon Card Collection</h1>
    </body>
   
   <?php    
    $type = $_REQUEST['t'];

    include('connection.php');

    $sql = "SELECT 
                *
            FROM
                pokemon p
                    INNER JOIN
                pokedex_type pt ON p.pokedex = pt.pid
                    INNER JOIN
                type t ON pt.tid = t.id
            WHERE
                t.id = " . $type;

   $result = $conn -> query($sql);

   	if (!$result) {
   		die("Database access failed: " . mysqli_error());
    }
    //output error message if query execution failed

   $rows = mysqli_num_rows($result);
    // get number of rows returned

   	if ($rows) {

        while ($row = mysqli_fetch_array($result)) {
        ?>
            <div class = "card">
                Name: <?=$row['name']?> <br>
                HP:  <?=$row['hp']?>   <br>
                ATK: <?=$row['atk']?><br>
                SAT: <?=$row['sat']?><br>
                SDF: <?=$row['sdf']?><br>
                SPD: <?=$row['spd']?><br>
                Type: <?=$row['type']?><br>
            </div>
      <?php
        }
   }
   mysqli_close($conn);
?>
<div class = "back">
    <a href="pokemon.php">Back</a>
</div>



</html>
