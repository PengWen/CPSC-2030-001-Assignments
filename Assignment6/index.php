<?php

   // -Load the file
   // -store it as a string
   // -Process the string, by replacing templated values
   // -echo the string

   require_once 'sqlhelper.php';
   require_once './vendor/autoload.php'; 

   //include the twig library.
   $loader = new Twig_Loader_Filesystem('./templates');
   //set to load from the ./templates directory

    //Sometimes you have to manually delete the cache
    $twig = new Twig_Environment($loader);


    //Sql setup
    $conn = connectToMyDatabase();


    if(isset($_REQUEST["edit"]) && isset($_REQUEST["fav"])) {
        $pokemonId = $_REQUEST["edit"];
        $favorite = $_REQUEST["fav"];
        
        $sql = updatePokemonSQL($pokemonId, $favorite);
        $result = $conn->query($sql);
    }

    $sql = getPokemonsSQL(1);
    //echo $conn->connect_errono;
    $result = $conn->query($sql); 


    $sql = getPokemonsSQL(0);
    //echo $conn->connect_errono;
    $resultNonFavorite = $conn->query($sql); 

    //Simple example of routing
    //I can load a different page, if there is an error
    if($result && $resultNonFavorite){
      $favorites = $result->fetch_all(MYSQLI_ASSOC); 
      $collection = $resultNonFavorite->fetch_all(MYSQLI_ASSOC);
      //setup twig
      $template = $twig->load('mainpage.twig.php');
 
      //call render to replace values in template with ones specified in my array
      //Since the return value is a string, I can echo it.
      echo $template->render(array("pokedex"=>$favorites, "pokemonCollection"=>$collection));

      $conn->close(); 
        
   }else {
    
    //One benefit is that we can load a full error page
    $template = $twig->load("error.twig.html");
    echo $template->render(array("message"=>"SQL errorm query failed"));
   }


function getPokemonsSQL($favorite = 0) {
     $sql = "SELECT p.*
            FROM
            pokemon p
            where p.favourite = " . $favorite;
    
    return $sql;
}

function updatePokemonSQL($pokedex, $favorite){
    $status = 0;
    
    if($favorite == 0) {
        $status = 1;
    }
    
    $sql = "UPDATE pokemon SET favourite = '". $status ."' WHERE pokemon.pokedex = " . $pokedex;
    return $sql;
}


?>
<!-- no more html here -->
<!-- leads to cleaner code -->
