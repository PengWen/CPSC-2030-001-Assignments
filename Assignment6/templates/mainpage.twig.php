<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js"></script>
    <title>My Favorite Pokemon</title>
</head>

<body>

    <header>
        <h1>My Favorite Pokemon</h1>
    </header>


    <div class="wrapper">
        <!--navigation: contain links to popular Gen 4 Poke,on. This twig should accept enough parameters to create this link menu, that is no link should be hard coded in the twig itself.-->
        <div class="sidebar">




        </div>

        <!--collection of cards-->
        <!--clicking on pokemon should add to favorite-->


        <div class="pokemonTable">
            <h3>Favorite</h3>

            {%for pokemon in pokedex %}
            <div class="list">
                <div class="pokedex">Pokedex: {{pokemon.pokedex}}</div>
                <div class="name">Name: {{pokemon.name}}</div>
                <div class="hp">Hp: {{pokemon.hp}}</div>
                <div class="atk">Atk: {{pokemon.atk}}</div>
                <div class="def">Def: {{pokemon.def}}</div>
                <div class="sat">Sat: {{pokemon.sat}}</div>
                <div class="sdf">Sdf: {{pokemon.sdf}}</div>
                <div class="spd">Spd: {{pokemon.spd}}</div>
                <div class="bst">Bst: {{pokemon.bst}}</div>
                <hr>
                <div><a href="index.php?edit={{pokemon.pokedex}}&fav={{pokemon.favourite}}"><button>delete</button></a></div>
            </div>
            {% endfor %}

            <!--favorite card display here-->
            <!--favorite pokemon displayed as cards should be a way to remove from favorites-->
            <div class="favorite">
                <h3>Collection</h3>
                <table>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Hp </th>
                        <th>Atk</th>
                        <th>Def</th>
                        <th>Sat</th>
                        <th>Sdf</th>
                        <th>Spd</th>
                        <th>Bst</th>
                    </tr>
                    {%for pokemon in pokemonCollection %}
                    <tr>
                        <td><a href="index.php?edit={{pokemon.pokedex}}&fav={{pokemon.favourite}}"><button>add</button></a></td>
                        <td>{{pokemon.name}}</td>
                        <td>{{pokemon.hp}}</td>
                        <td>{{pokemon.atk}}</td>
                        <td>{{pokemon.def}}</td>
                        <td>{{pokemon.sat}}</td>
                        <td>{{pokemon.sdf}}</td>
                        <td>{{pokemon.spd}}</td>
                        <td>{{pokemon.bst}}</td>
                    </tr>

                    {% endfor %}

                </table>
            </div>
        </div>
    </div>


</body>

</html>
