let char01 = {
    name: "Claude Wallace",
    side: "Edinburgh Army",
    unit: "Ranger Corps, Squad E",
    rank: "First Lieutenant",
    role: "Tank Commander",
    description: "Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.",
    imageUrl: "http://valkyria.sega.com/img/character/img01.png"
}


let char02 = {
    name: "Riley Miller",
    side: "Edinburgh Army",
    unit: "Federate Joint Ops",
    rank: "Second Lieutenant",
    role: "Artillery Advisor",
    description: "Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones.",
    imageUrl: "http://valkyria.sega.com/img/character/img02.png"
}


let char03 = {
    name: "Raz",
    side: "Edinburgh Army",
    unit: "Ranger Corps, Squad E",
    rank: "Sergeant",
    role: "Fireteam Leader",
    description: "Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible.",
    imageUrl: "http://valkyria.sega.com/img/character/img03.png"
}

let char04 = {
    name: "Kai Schulen",
    side: "Edinburgh Army",
    unit: "Ranger Corps, Squad E",
    rank: "Sergeant Major",
    role: "Fireteam Leader",
    description: "Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename \"Deadeye Kai\". Along with her childhood friends, she joined a foreign military to take the fight to the Empire.She loves fresh - baked bread, almost to a fault.",
    imageUrl: "http://valkyria.sega.com/img/character/img04.png"
}


let char05 = {
    name: "Angelica Farnaby",
    side: "N/A",
    unit: "N/A",
    rank: "N/A",
    role: "N/A",
    description: "A chipper civilian girl who stumbled upon Squad E through strange circumstances. Nicknamed \"Angie,\" she is beloved by the entire squad for her eagerness to help. She seems to be suffering from amnesia, and can only remember her own name.",
    imageUrl: "http://valkyria.sega.com/img/character/img14.png"
}

let char06 = {
    name: "Minerva Victor",
    side: "Edinburgh Army",
    unit: "Ranger Corps, Squad E",
    rank: "First Lieutenant",
    role: "Senior Commander",
    description: "Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.",
    imageUrl: "http://valkyria.sega.com/img/character/img11.png"
}

let char07 = {
    name: "Karen Stuart",
    side: "Edinburgh Army",
    unit: "Squad E",
    rank: "Corporal",
    role: "Combat EMT",
    description: "Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household.",
    imageUrl: "http://valkyria.sega.com/img/character/img12.png"
}

let char08 = {
    name: "Ragnarok",
    side: "Edinburgh Army",
    unit: "Squad E",
    rank: "K-9 Unit",
    role: "Mascot",
    description: "Once a stray, this good good boy is lovingly referred to as Rags. As a K-9 unit, he's a brave and intelligent rescue dog who's always willing to lend a helping paw. When the going gets tough, the tough get ruff.",
    imageUrl: "http://valkyria.sega.com/img/character/img13.png"
}

let char09 = {
    name: "Miles Arbeck",
    side: "Edinburgh Army",
    unit: "Ranger Corps, Squad E",
    rank: "Sergeant",
    role: "Tank Operator",
    description: "Once a stray, this good good boy is lovingly referred to as \"Rags.\"Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby.",
    imageUrl: "http://valkyria.sega.com/img/character/img15.png"
}

let char10 = {
    name: "Dan Bentley",
    side: "Edinburgh Army",
    unit: "Ranger Corps, Squad E",
    rank: "Private First Class",
    role: "APC Operator",
    description: "Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat.",
    imageUrl: "http://valkyria.sega.com/img/character/img16.png"
}

var animationCharacters = [char01, char02, char03, char04, char05, char06, char07, char08, char09, char10];

function getCharNames() {
    var nameArray = new Array();

    for (var count = 0; count < animationCharacters.length; count++) {
        nameArray.push(animationCharacters[count].name);
    }
    return nameArray;
}

function getAnimationCharacter(name) {
    for (let count = 0; count < animationCharacters.length; count++) {
        if (animationCharacters[count].name == name) {
            return animationCharacters[count];
        }
    }
}

function getAnimationProfile(name) {
    for (let count = 0; count < animationCharacters.length; count++) {
        if (animationCharacters[count].name == name) {
            return animationCharacters[count];
        }
    }
}
