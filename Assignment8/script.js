$(document).ready(function () {
    var isOpen = false;

    $(".menu").click(function () {
        if (!isOpen) {
            $(this).css("transform", "rotate(-45deg)");
            $(this).css("transform-origin", "top left");
            $(this).css("transition", "2s");

            $("#test").addClass("item-open");
        } else {
            $(this).css("transform", "rotate(0deg)");
            $(this).css("transform-origin", "top left");
            $(this).css("transition", "2s");
            
            $("#test").removeClass("item-open");
        }
        isOpen = !isOpen;
    });

    $(".item1").click(function () {
        $(this).css("animation-name", "circle");
    });

});
