<?php
include("queries.php");

if($_REQUEST["action"] === "sendMessage") {
    $username = $_REQUEST["username"];
    $message = $_REQUEST["message"];

    insertMessage($username,$message);
}
else if($_REQUEST["action"] === "recentMessages") {

    $result = getMostRecentMessages();
    $output = array();
    
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            array_push($output,array(
                "time"=>$row["message_time"],
                "user"=>$row["username"],
                "message"=>$row["message"]));            
        }
    }   
    echo json_encode($output);    
}
else if($_REQUEST["action"] === "messageAfter") {
    
    $time = $_REQUEST["time"];    
    $result = getMessageAtTime($time);
    $output = array();
    
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            array_push($output,array(
                "time"=>$row["message_time"],
                "user"=>$row["username"],
                "message"=>$row["message"]));            
        }
    }   
    echo json_encode($output);       
}
