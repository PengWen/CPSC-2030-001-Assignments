function createContent(data) {
    let msg = "";

    for (var index = 0; index < data.length; index++) {
        msg += "<b>Time:</b> " + data[index].time + "<br>" +
            "<b>User:</b>" + data[index].user + "<br>" +
            "<b>Message:</b>" + data[index].message + "<br>";
    }
    let result = "<div>" + msg + "</div>";
    return result;
}

$("#submit").click(function (event) {

    let username = $("#username").val();
    let message = $("#message").val();

    $.ajax({
        url: "backend.php",
        method: "POST",
        data: {
            action: "sendMessage",
            username: username,
            message: message
        },
        success: function () {
            $("#result").html("Message sent!");
        }
    });
});

$("#recentMessages").click(function (event) {
    $.ajax({
        url: "backend.php",
        method: "POST",
        data: {
        action: "recentMessages"
        },
        success: function (result) {
            let data = JSON.parse(result);
            $("#result").html(createContent(data));
        }
    });
});
