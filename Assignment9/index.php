<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Assignment9 - chatroom</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="functions.js" defer></script>
</head>

<body>
    <h1>ChatRoom</h1>
    <form>
        <label for="username">Username:</label>
        <input type="text" id="username" name="username">
        <br><br>
        <label for="message">Message:</label>
        <textarea name="message" id="message" rows="3" cols="50"></textarea>
        <br><br>
        <input type="button" id="submit" value="Submit">
        <br><br>
        <input type="button" id="recentMessages" value="Show Recent Messages">
        <br><br>
        <input type="button" id="newMessages" value="Get New Messages">
    </form>

    <div id="result"></div>

    <form method="post" action="backend.php">
        <input type="hidden" name="action" value="messageAfter">
        <label for="time">Time:</label>
        <input type="datetime-local" id="time" name="time">
        <input type="button" value="Get New Messages">
    </form>
</body>

</html>
