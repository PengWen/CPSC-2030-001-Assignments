<?php
include('header.php');
?>

<body>
    <header class="header-about">
        <nav>
            <div class="row">
                <a href="index.php">
                    <img src="image/Hospital_Logo.png" alt="Hospital logo" class="logo">
                </a>
                <ul class="main-nav">
                    <li><a href="login.php">Login</a></li>
                    <li><a href="about.php">About</a></li>
                    <li><a href="reservation.php">Reservation</a></li>
                    <li><a href="blog.php">Blog</a></li>
                    <li><a href="faq.php">FAQ</a></li>
                </ul>
            </div>
        </nav>

    </header>

    <div class="aboutContent">
        <h2>Welcome to <u>Masako Hospital!</u></h2><br>
        Masako Hospital is a health care institution providing patient treatment with specialized in pediatric, woman and medicine services. We are committed to help children and their families receive the high-quality of care and live the healthiest possible lives. The geographic regions served include the southeastern portion of Japan, Hokkaido, as well as the central and north Japan. We build an advanced approach to pediatric therapies, innovative research, and community outreach, and make sure people from around the world can depend on us. In order for us to serve you better, you can click on the tab “Reservation”, to make an appointment with us, or you can simply contact us by the information underneath.<br>

        <br>
        Tel #: 00813-928-038
        <br>
        Address: 8372 Hilamia Street, Tokyo
        <br>
        Opening Hours: Mon-Sat 09:00-20:00
    </div>


    <?php
include('footer.php');
?>
