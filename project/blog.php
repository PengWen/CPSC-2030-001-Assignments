<?php
include('header.php');
?>

<body>
    <header class="header-blog">
        <nav>
            <div class="row">
                <a href="index.html">
                    <img src="image/Hospital_Logo.png" alt="Hospital logo" class="logo">
                </a>
                <ul class="main-nav">
                    <li><a href="login.php">Login</a></li>
                    <li><a href="about.php">About</a></li>
                    <li><a href="reservation.php">Reservation</a></li>
                    <li><a href="blog.php">Blog</a></li>
                    <li><a href="faq.php">FAQ</a></li>
                </ul>
            </div>
        </nav>

    </header>

    <?php
    include ("connection.php");
    
    $sql = "SELECT * from blog";
    $result = $conn->query($sql);
    
    echo "<div class='blog'>";
    while($row = $result->fetch_assoc()) {
       
        echo "<h3>" . $row["title"] . "</h3>";
        echo "<p>" . $row["content"] . "</p>";
}
        echo "</div>";
    $conn->close();

    
    ?>

    <?php
include('footer.php');
?>
