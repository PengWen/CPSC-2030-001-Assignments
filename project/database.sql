-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 04, 2018 at 08:27 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `hospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `reason` text NOT NULL,
  `language` varchar(30) NOT NULL,
  `user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `title` text NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`title`, `content`) VALUES
('2 tips to maintain good mental health', '1.Eat well!\r\nDiet plays a crucial role in mental health; it’s important to have a healthy diet. If you need help with this, consult the Canadian Food Guide. You can also consult a nutritionist for a personalized guide adapted to your needs. 2.Exercise regularly.\r\nPhysical exercise plays a positive role in your mental health. It causes chemical reactions that are proven to reduce anxiety and stress and put you in a good mood.');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `question`, `answer`) VALUES
(1, 'What if my reservation time is not available?', 'No worries! Masako Hospital will call you and schedule up a new reservation for you.'),
(2, 'How can I choose my prefer doctor in the reservation page?', 'Masako Hospital will know which doctor you prefer by your previous experiences. You will also be ask to do the survey after you visit our doctor each time.'),
(3, 'As a walk-in, how long is the waiting time?', 'Approximately 30-45 minutes, and with maximum 2 hours.'),
(4, 'What is Medical Services Plan(MSP)? And what does it cover?', 'Medical Services Plan (MSP) is British Columbia’s (BC) provincial health insurance plan, mandatory for everyone who lives in the province for six months or longer. IF you will reside in BC for six months or longer, you should apply for MSP immediately after you arrive. It covers basic medical care within Canada, most doctor visits, most hospital visits.');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`) VALUES
(1, 'test', 'test@test.com', '$2y$10$Asq2BdGKAv7fqnsT8k8i1ewXAd8BKOwMataa/qSatVnrbiH7poS6C');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `FK_USER` (`user`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointment`
--
ALTER TABLE `appointment`
  ADD CONSTRAINT `FK_USER_APP` FOREIGN KEY (`user`) REFERENCES `user` (`id`);
