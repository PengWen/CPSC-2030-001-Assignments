<?php
include('header.php');
?>

<body>
    <header class="header-faq">
        <nav>
            <div class="row">
                <a href="index.php">
                    <img src="image/Hospital_Logo.png" alt="Hospital logo" class="logo">
                </a>
                <ul class="main-nav">
                    <li><a href="login.php">Login</a></li>
                    <li><a href="about.php">About</a></li>
                    <li><a href="reservation.php">Reservation</a></li>
                    <li><a href="blog.php">Blog</a></li>
                    <li><a href="faq.php">FAQ</a></li>
                </ul>
            </div>
        </nav>

    </header>

    <?php
    include ("connection.php");
    
    $sql = "SELECT * from faq";
    $result = $conn->query($sql);

    echo "<div class='faq'>";
    while($row = $result->fetch_assoc()) {
       
        echo "<h2>" . $row["question"] . "</h2>";
        echo "<p>" . $row["answer"] . "</p>";
      
}
        echo "</div>";
    $conn->close();
    ?>


    <?php
include('footer.php');
?>
