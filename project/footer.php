<footer>
    <ul class="footer-nav">
        <li><a href="#">About us</a></li>
        <li><a href="#">Blog</a></li>
        <li><a href="#">Press</a></li>
        <li><a href="#">IOS App</a></li>
        <li><a href="#">Android App</a></li>
    </ul>
    <ul class="social-links">
        <li><a href="#"><i class="ion-logo-facebook"></i></a></li>
        <li><a href="#"><i class="ion-logo-snapchat"></i></a></li>
        <li><a href="#"><i class="ion-logo-whatsapp"></i></a></li>
        <li><a href="#"><i class="ion-logo-instagram"></i></a></li>
    </ul>
    <p>
        Copyright &copy; 2018 by Hostpital. All rights reserved.
    </p>
</footer>

</html>
