<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Masako Hospital</title>
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
    <!--    <link rel="stylesheet" href="grid.css">-->
    <link rel="stylesheet" href="normalize.css">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:300,400,700" rel="stylesheet">
    <link href="https://unpkg.com/ionicons@4.4.4/dist/css/ionicons.min.css" rel="stylesheet">
</head>
