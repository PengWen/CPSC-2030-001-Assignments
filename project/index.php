<?php
include('header.php');
?>

<body>
    <header class="header-index">
        <nav>
            <div class="row">
                <a href="index.php">
                    <img src="image/Hospital_Logo.png" alt="Hospital logo" class="logo">
                </a>
                <ul class="main-nav">
                    <li><a href="login.php">Login</a></li>
                    <li><a href="about.php">About</a></li>
                    <li><a href="reservation.php">Reservation</a></li>
                    <li><a href="blog.php">Blog</a></li>
                    <li><a href="faq.php">FAQ</a></li>
                </ul>
            </div>
        </nav>
        <div class="hero-text-box">
            <h1>“Only a life lived to the<br> service of others is worth living.”
                <br>--Albert Einstein
            </h1>
            <a class="btn btn-full" href="login.php">Login</a>
            <a class="btn btn-ghost" href="about.php">About Us</a>
        </div>
    </header>

</body>

<?php
include('footer.php');
?>
