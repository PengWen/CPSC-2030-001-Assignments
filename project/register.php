<?php
include('header.php');
?>

<body>
    <header class="header-login">
        <nav>
            <div class="row">
                <a href="index.php">
                    <img src="image/Hospital_Logo.png" alt="Hospital logo" class="logo">
                </a>
                <ul class="main-nav">
                    <li><a href="login.html">Login</a></li>
                    <li><a href="about.html">About</a></li>
                    <li><a href="reservation.html">Reservation</a></li>
                    <li><a href="blog.html">Blog</a></li>
                    <li><a href="faq.html">FAQ</a></li>
                </ul>
            </div>
        </nav>

    </header>

    <form>
        Username:<br>
        <input id="username" type="text" name="username"><br><br>
        Email:<br>
        <input id="email" type="email" name="email">
        <br><br>
        Password:<br>
        <input id="password" type="password" name="password_1">
        <br><br>
        Confirm Password:<br>
        <input id="confirmPassword" type="password" name="password_2">
        <br><br>
        <a href="#">Register</a>

    </form>

    <?php
include('footer.php');
?>
