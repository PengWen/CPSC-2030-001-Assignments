<?php
include('header.php');
?>

<body>
    <header class="header-reservation">
        <nav>
            <div class="row">
                <a href="index.php">
                    <img src="image/Hospital_Logo.png" alt="Hospital logo" class="logo">
                </a>
                <ul class="main-nav">
                    <li><a href="login.php">Login</a></li>
                    <li><a href="about.php">About</a></li>
                    <li><a href="reservation.php">Reservation</a></li>
                    <li><a href="blog.php">Blog</a></li>
                    <li><a href="faq.php">FAQ</a></li>
                </ul>
            </div>
        </nav>

    </header>

    <form>
        Email:<br>
        <input type="email" name="email"><br><br>
        Date:<br>
        <input type="date" name="date"><br><br>
        Time:<br>
        <input type="time" name="time"><br><br>
        Preferred Language:
        <select>
            <option value="mandarin">Mandarin</option>
            <option value="english">English</option>
            <option value="french">French</option>
            <option value="spanish">Spanish</option>
        </select><br>
        Reason:<br>
        <textarea rows="4" cols="23"></textarea>
        <hr>
        <a href="#" class="submit">Submit</a><br>
    </form>


    <?php
include('footer.php');
?>
