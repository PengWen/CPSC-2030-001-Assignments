$("#submit").click(function (event) {

    let username = $("#username").val();
    let message = $("#message").val();

    $.ajax({
        url: "register.php",
        method: "POST",
        data: {
            action: "sendMessage",
            username: username,
            message: message
        },
        success: function () {
            $("#result").html("Message sent!");
        }
    });
});
